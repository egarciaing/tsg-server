import { GraphQLSchema, defaultFieldResolver, GraphQLNamedType } from 'graphql';
import { mapSchema, getDirectives, MapperKind } from '@graphql-tools/utils';
import GqlError from '../../../lib/GqlError';
import { AuthDirective, AuthResolverFn } from '../../../lib/auth/Auth';
import AuthResponse from '../../../lib/auth/AuthResponse';

type Context = AuthResponse & Record<string, never>;

function authenticationDirective(directiveName = "auth", contextResolver: AuthResolverFn<Context>): AuthDirective {
  const typeDirectiveArgumentMaps: Record<string, unknown> = {};
  const typeDefinitions: Record<string, GraphQLNamedType> = {};

  const DirectiveTransformer = (schema: GraphQLSchema) => mapSchema(schema, {
    [MapperKind.TYPE]: (type) => {
      const typeDirectives = getDirectives(schema, type);
      typeDirectiveArgumentMaps[type.name] = typeDirectives[directiveName];
      typeDefinitions[type.name] = type;

      return undefined;
    },
    [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
      const fieldDirectives = getDirectives(schema, fieldConfig);
      const directiveArgumentMap = fieldDirectives[directiveName] ?? typeDirectiveArgumentMaps[typeName];

      if (directiveArgumentMap) {
        const { resolve = defaultFieldResolver } = fieldConfig;

        fieldConfig.resolve = (source, args, context, info) => {
          const accdenied = new GqlError("Access Denied", "ErrAccessDenied");
          context = contextResolver(context);

          if (context.auth?.info) {
            accdenied.setTrace(context.auth.info);
            accdenied.trace = [];
            return accdenied;
          }

          if (context.auth?.error) {
            accdenied.setTrace(context.auth.error);
            accdenied.trace = [];
            return accdenied;
          }

          if (!context.user) {
            accdenied.trace = [];
            return accdenied;
          }

          return resolve(source, args, context, info);
        }

        return fieldConfig;
      }
    }
  });

  return {
    TypeDefs: `directive @${directiveName} on OBJECT | FIELD_DEFINITION`,
    DirectiveTransformer
  };
}

export default authenticationDirective;
