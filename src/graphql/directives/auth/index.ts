import authentication from "./authentication";
import authorization from "./authorization";

export {
    authentication as authenticationDirective,
    authorization as authorizationDirective
}