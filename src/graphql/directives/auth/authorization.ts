import { GraphQLSchema, defaultFieldResolver, GraphQLNamedType } from 'graphql';
import { mapSchema, getDirectives, MapperKind, DirectiveUseMap } from '@graphql-tools/utils';
import { AuthDirective, AuthResolverFn } from "../../../lib/auth/Auth";
import AuthResponse from '../../../lib/auth/AuthResponse';
import GqlError from '../../../lib/GqlError';
// import GqlError from '../../GqlError';

type AuthorizeMap = {
    role?: string | null,
    permissions?: Set<string>
};

type Context = { authorize: AuthorizeMap | null } & AuthResponse & Record<string, never>;

/**
 * @param string token
 * 
 * @example 
 * retrievePermissionsMap("Role:*") // all permissions
 * retrievePermissionsMap("Role:permission1") // single permission
 * retrievePermissionsMap("Role:permission1,permission2,permission3") // multiple directed permissions
 * retrievePermissionsMap("Role,Role0:permission1") // retrieve the array of roles
 * 
 * @returns AuthorizeMap
 */
function retrievePermissionsMap(token: string): AuthorizeMap {
    const entitySplitter = ":", keySplitter = ",";
    const map: AuthorizeMap = {
        role: null
    };

    const cleaner = (value: string) => value && value.trim() !== "";
    const entites = token.split(entitySplitter)
        .filter(cleaner)
        .map(entity => entity.split(keySplitter))
        .filter(map => map.filter(cleaner))
        .filter(map => map.filter((v, i) => map.indexOf(v) === i));

    if (/*has role entity?*/token.indexOf(":") > -1) {
        map.role = entites[0][0];
        map.permissions = new Set(entites[1]);
    } else {

        map.permissions = new Set(entites[0]);
    }

    return map;
}

function authorizationDirective(directiveName = "authorize", contextResolver: AuthResolverFn<Context>): AuthDirective {
    const typeDirectiveArgumentMaps: DirectiveUseMap = {};
    const typeDefinitions: Record<string, GraphQLNamedType> = {};

    const DirectiveTransformer = (schema: GraphQLSchema) => mapSchema(schema, {
        [MapperKind.TYPE]: (type) => {
            const typeDirectives = getDirectives(schema, type);
            typeDirectiveArgumentMaps[type.name] = typeDirectives[directiveName];
            typeDefinitions[type.name] = type;

            return undefined;
        },
        [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
            const fieldDirectives = getDirectives(schema, fieldConfig);
            const directiveArgumentMap: DirectiveUseMap = fieldDirectives[directiveName] ?? typeDirectiveArgumentMaps[typeName];

            if (directiveArgumentMap) {
                const { name = null } = directiveArgumentMap as { name?: string };
                const { resolve = defaultFieldResolver } = fieldConfig;

                fieldConfig.resolve = (source, args, context, info) => {
                    const permissionMap: AuthorizeMap = retrievePermissionsMap(name);
                    context.authorize = permissionMap;
                    try {
                        context = contextResolver(context);

                        if (!context.authorize) {
                            const error = new GqlError("Access Denied", "ErrUnauthorized");
                            error.setTrace(new Error("User Not Authorized"));
                            return error;
                        }
                    } catch (error) {

                        const gerror = new GqlError("Access Denied", "ErrUnauthorized");
                        gerror.setTrace(error);
                        return gerror;
                    }

                    return resolve(source, args, context, info);
                };

            }
            return fieldConfig;
        }
    });

    return {
        DirectiveTransformer,
        TypeDefs: `directive @${directiveName} {
            name: !String
        } on OBJECT | FIELD_DEFINITION`
    };
}

export default authorizationDirective;