import lo from "lodash";
import { Arg, Authorized, Ctx, FieldResolver, Query, Resolver, Root } from "type-graphql";
import Person from "../../db/models/Org/People";
import Tenant from "../../db/models/Org/Tenant";
import { GrapgQLContext } from "../../lib/graphql";

@Resolver(() => Person)
export class PersonResolver {
    
    @FieldResolver(() => String)
    fullname(@Root() parent: Person): string {
        return lo.capitalize([parent?.firstname, parent?.lastname].filter(name => name && name.length > 0).join(" "));
    }

    @FieldResolver(() => String, { nullable: true })
    async tenant(@Root() parent: Person): Promise<string | null> {
        return Promise.resolve(parent.tenant?.token).then(token => {
            if (token) {
                return token;
            }

            if (!parent?.tenantId) {
                return null
            }

            return Tenant.findByPk(parent.tenantId, { attributes: ["token"] }).then(tenant => tenant.token);
        });
    }

    @Authorized()
    @Query(() => Person)
    async person(
        @Arg("id", { nullable: true, defaultValue: null }) id: number = null,
        @Ctx() context: GrapgQLContext<unknown, { owner: Person }>
    ): Promise<Person> {
        const defaultId = context.user?.owner.id;
        return Person.findByPk(id ?? defaultId, { include: [] });
    }

    @Authorized()
    @Query(() => [Person], { nullable: "items" })
    async people(
        @Arg("limit", { defaultValue: 20 }) limit: number,
        @Arg("offset", { defaultValue: 0 }) offset: number,
        @Ctx() context: GrapgQLContext<unknown, { owner: Person }>
    ): Promise<Person[]> {
        const tenantId = context.user?.owner.tenantId;

        return Person.findAll({
            limit,
            offset,
            where: {
                tenantId
            }
        });
    }
}


