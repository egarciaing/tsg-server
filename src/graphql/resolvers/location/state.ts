import { FieldResolver, Resolver, Root } from "type-graphql";
import State from "../../../db/models/Location/State";
import Country from "../../../db/models/Location/Country";

@Resolver(() => State)
export class StateResolver {
    @FieldResolver(() => Country, { nullable: true })
    async state(@Root() parent: State): Promise<Country | null> {
        return Promise.resolve(parent?.country).then(state => {
            if (state) {
                return state
            }

            if (!parent?.country_id) {
                return null
            }

            return Country.findByPk(parent.country_id);
        })
    }
}
