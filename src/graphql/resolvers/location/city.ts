import { FieldResolver, Resolver, Root } from "type-graphql";
import City from "../../../db/models/Location/City";
import State from "../../../db/models/Location/State";

@Resolver(() => City)
export class CityResolver {
    @FieldResolver(() => State, { nullable: true })
    async state(@Root() parent: City): Promise<State | null> {
        return Promise.resolve(parent?.state).then(state => {
            if (state) {
                return state
            }

            if (!parent?.state_id) {
                return null
            }

            return State.findByPk(parent.state_id);
        })
    }
}
