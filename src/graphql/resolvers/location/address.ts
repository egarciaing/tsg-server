import { FieldResolver, Resolver, Root } from "type-graphql";
import Address from "../../../db/models/Location/Address";
import City from "../../../db/models/Location/City";

@Resolver(() => Address)
export class AddressResolver {
    @FieldResolver(() => City, { nullable: true })
    async city(@Root() parent: Address): Promise<City | null> {
        return Promise.resolve(parent?.city).then(city => {
            if (city) {
                return city
            }

            if (!parent?.cityId) {
                return null
            }

            return City.findByPk(parent.cityId);
        })
    }
}

