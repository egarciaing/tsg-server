import { Arg, Authorized, FieldResolver, Mutation, Query, Resolver, Root } from "type-graphql";
import User from "../../db/models/User";
import Person from "../../db/models/Org/People";
import Tenant from "../../db/models/Org/Tenant";

@Resolver(() => User)
export class UserResolver {

    @FieldResolver(() => String)
    async tenant(@Root() parent: User): Promise<string | null> {
        return Promise.resolve(parent?.owner?.tenantId).then(id => {
            if (!id) {
                return null
            }

            return Tenant.findByPk(id, { attributes: ["token"] }).then(tenant => tenant.token);
        });
    }

    @Query(() => User, {
        description: "Getting user resolution by the email"
    })
    async user(@Arg("email") email: string): Promise<User> {
        return User.findOne({
            where: {
                email
            }
        });
    }

    @Authorized(["root", "admin"])
    @Query(() => [User], {
        nullable: "items",
        description: "Getting all user list available scope to you"
    })
    async users(
        @Arg("limit", { defaultValue: 20 }) limit: number,
        @Arg("offset", { defaultValue: 0 }) offset: number
    ): Promise<User[]> {
        return User.findAll({
            include: [{ model: Person }],
            limit, offset
        });
    }

    @Authorized(["root", "admin"])
    @Mutation(() => Number)
    async deleteUser(@Arg("id") id: number): Promise<number> {
        return User.destroy({ 
            where:{
                id
            }
        });
    }
}
