import Country from "../../db/models/Location/Country";
import { FieldResolver, Resolver, Root } from "type-graphql";
import Phone from "../../db/models/Phone";

@Resolver(() => Phone)
export class PhoneResolver {
    @FieldResolver(() => Number)
    code(@Root() parent: Phone): Promise<number | null> {
        return Promise.resolve(parent.country?.phonecode).then(code => {
            if (code) {
                return code;
            }
            if (!parent?.countryId) {
                return null;
            }

            return Country
                .findByPk(parent.countryId, { attributes: ["phonecode"] })
                .then(country => country.phonecode);
        })
    }
}

