import { Resolver, Mutation, Arg, Query, Ctx, ObjectType, Field, Authorized } from "type-graphql";

import AuthStrategy from "../../middleware/auth/strategy";
import { GrapgQLContext } from "../../lib/graphql";
import { IAuth, IUser } from "../../lib/auth";
import User from "../../db/models/User";

@ObjectType()
export class Auth implements IAuth {
    @Field(() => User)
    user: IUser;

    @Field(() => String)
    token: string;

    constructor(user: IUser, token: string) {
        this.user = user;
        this.token = token;
    }
}

@Resolver(()=> Auth)
export class AuthResolver {
    @Authorized()
    @Query(() => Auth)
    me(@Ctx() context: GrapgQLContext): IAuth {
        return new Auth(context.user, context.auth?.token);
    }

    @Mutation(() => Auth)
    async login(
        @Arg("email") email: string,
        @Arg("password") password: string
    ): Promise<IAuth> {
        return AuthStrategy.loginWithPasswort(email, password);
    }
}