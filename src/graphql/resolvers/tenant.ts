import { FieldResolver, Query, Resolver, Root, Arg, Ctx } from "type-graphql";
import Tenant from "../../db/models/Org/Tenant";

@Resolver(() => Tenant)
export class PersonResolver {
    @FieldResolver(() => String)
    token(@Root() parent: Tenant): string {
        return parent.token;
    }

    @Query(() => Tenant)
    async tenant(
        @Arg("id", { nullable: true, defaultValue: null }) id: number = null,
        @Ctx() context: GrapgQLContext<unknown, { owner: Person }>
    ): Promise<Tenant | null> {
        return 
    }

    @Query(() => [Tenant], { nullable: "items" })
    async tenants(
        @Arg("limit", { defaultValue: 20 }) limit: number,
        @Arg("offset", { defaultValue: 0 }) offset: number,
        @Ctx() context: GrapgQLContext<unknown, { owner: Person }>
    ): Promise<Tenant | null> {
        return
    }
}

