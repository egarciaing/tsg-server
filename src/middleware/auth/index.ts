import passport from "passport";
import { Application } from "express";
import { Encoder } from "../../lib/auth";
import Auth from "./strategy";

export default function AuthMiddleware(app: Application): typeof Auth {

    passport.serializeUser(Encoder.serialize());
    passport.deserializeUser(Auth.strategy.deserialize());

    passport.use(Auth.strategy.createLocalStrategy());
    passport.use(Auth.strategy.createJwtStrategy());

    app.use(passport.initialize());
    app.use(passport.session());
    app.use(Auth.middleware);

    return Auth;
}