import { Strategy, Authentication } from "../../lib/auth";
import User from "../../db/models/User";
import { IUser } from "../../lib/auth";
import Person from "../../db/models/Org/People";

// auth passport
export default new Authentication(new Strategy({
    finder(email) {
        return User.findOne({
            where: { email },
            include: [{ model: Person }]
        }).then(user => user.toJSON() as IUser).then(user => {
            return user;
        })
    }
}));