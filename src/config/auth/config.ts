import { ExtractJwt, StrategyOptions } from 'passport-jwt';
import { IStrategyOptions } from 'passport-local';
import { Algorithm, SignOptions } from 'jsonwebtoken';
import env from '../env';

const secretOrKey: string = env.JWT_SECRET || 'secretlife';
const defaultAlgorithm: Algorithm = env.JWT_ALGORITHM ? (env.JWT_ALGORITHM) : "HS256";
export const JWT_LIFETIME = env.JWT_LIFETIME && !isNaN(parseInt(env.JWT_LIFETIME)) ? parseInt(env.JWT_LIFETIME) : Math.floor(Date.now() / 1000) + (60 * 60)

export const JWT_SIGN_TOKEN: { secret: string } & SignOptions = {
    algorithm: defaultAlgorithm,
    expiresIn: JWT_LIFETIME,
    secret: secretOrKey
}

export const JWT_STRATEGY_OPTIONS: StrategyOptions = {
    secretOrKey,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    algorithms: [defaultAlgorithm],
    ignoreExpiration: false,
    jsonWebTokenOptions: {
        algorithms: [defaultAlgorithm],
    }
};

export const AUTH_OPTIONS: IStrategyOptions = {
    usernameField: 'email',
    passwordField: 'password',
    session: false
}

export const BLACK_BOX = [
    "hash",
    "salt",
    "activationKey",
    "resetPasswordKey"
];
