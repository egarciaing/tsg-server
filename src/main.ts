require("reflect-metadata");

import cors from "cors";
import path from "path";
import helmet from "helmet";
import morgan from "morgan";
import express from "express";
import session from "express-session";
import { graphqlHTTP } from "express-graphql";
import { json as parserJSON } from "body-parser";
import { makeExecutableSchema } from '@graphql-tools/schema';
import { buildTypeDefsAndResolvers } from "type-graphql";

import normalizePort from './lib/normalizePort';
import env, { isProdMode } from './config/env';
import db from "./db"

import {
    //loadResolverDirectories,
    loadTypeDirectories
} from "./lib/graphql";

import { AuthResponse } from "./lib/auth";
import AuthMiddleware from "./middleware/auth";
import { authChecker } from "./graphql/middleware/authChecker";

import {
    authenticationDirective,
} from "./graphql/directives/auth";

const host = env.HOST || `http://localhost`;
const port = normalizePort(env.PORT ?? '3000');
const app = express();

const authentication = authenticationDirective("auth", (context) => {
    return context;
});

app.use(cors({
    origin: [host]
}));

app.use(helmet({
    contentSecurityPolicy: false
}));

app.use(parserJSON());

app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: "my-secret"
}));

AuthMiddleware(app);

app.use(morgan("dev"));

(async () => {

    const { typeDefs, resolvers } = await buildTypeDefsAndResolvers({
        resolvers: [path.join(__dirname, "/graphql/resolvers", "/**/*.{ts,js}")],
        authChecker: authChecker
    })

    const schema = makeExecutableSchema({
        schemaTransforms: [authentication.DirectiveTransformer],
        resolvers: /*loadResolverDirectories(
            path.join(__dirname, "/graphql/resolvers/.raw.{ts,js}"), [
            resolvers
        ])*/resolvers,
        typeDefs: loadTypeDirectories(
            path.join(__dirname, "/graphql/typesDef"), [
            authentication.TypeDefs, typeDefs
        ])
    });

    app.use('/graphql', graphqlHTTP((request, response, params) => ({
        context: {
            ...(response as AuthResponse),
            request,
            params
        },
        schema: schema,
        graphiql: !isProdMode,
    })));

    app.listen(port, () => {
        const url = [host, port].join(":") + '/graphql';
        console.info(`Database name: ${db.config.database}`);
        console.info(`⚡️[${env.ENVIRONMENT} server]: Server is running at ${url}`);
    })
})()