
export default class GqlError extends Error {

    trace: string[];
    message: string;
    // tslint:disable-next-line: variable-name
    readonly __typename = "Error";

    constructor(message: string, name?: string, preview?: Error | GqlError) {
        super(message);
        this.name = name;

        this.setTrace(preview)
    }

    setTrace(preview: Error | GqlError): void {
        preview = (preview instanceof GqlError || preview instanceof Error) ? preview : null;

        const join = (a: string, b?: string) => [a].concat(b).filter(d => d && d.trim() !== "").join(' \n');
        const prepareStack = (stack: string) => {
            const _stack = stack?.split("at");
            _stack?.shift();
            return _stack;
        }

        this.trace = prepareStack(this.stack)
            .concat(
                [preview?.name && `[${preview.name}]`],
                prepareStack(preview?.stack)
            )
            .filter(d => !!d)
            .slice(0, 22);

        this.message = join(this.message, preview?.message);
    }
}
