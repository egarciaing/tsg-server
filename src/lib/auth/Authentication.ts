import jwt from 'jsonwebtoken';
import passport from 'passport';
import { Request, NextFunction } from 'express';

import { JWT_STRATEGY_OPTIONS } from '../../config/auth/config';
import GqlError from '../GqlError';

import makePayload from './makePayload';
import AuthResponse from './AuthResponse';
import Strategy from './Strategy';
import { Auth } from './Auth';
import { IUser } from './User';

export default class Authentication {

    debug = true;
    strategy: Strategy;

    constructor(strategy: Strategy) {
        this.strategy = strategy;
    }

    login = (require: Request, response: AuthResponse, next: NextFunction): void => {
        passport.authenticate("local", { session: false }, (error, user) => {
            if (error) {
                if (this.debug && error.message) console.error("authenticate jwt:", error.message);
                return next(error);
            }

            if (!user) {
                return next(new Error("User not found"))
            }

            const { data, ...config } = makePayload(user);
            jwt.sign(data, config.secret, { algorithm: config.algorithm }, (error, token) => {
                if (error) {
                    return next(error);
                }
                response.user = user;
                response.json({ data: { token, user } });
            })
        })(require, response, next);
    }

    loginWithPasswort = async (email: string, password: string): Promise<Auth> => {
        const auth = this.strategy.authenticate();
        return new Promise((res, rej) => {
            auth(email, password, (_op: unknown, user: IUser, error: Error) => {
                if (error) {
                    return rej(new Error(error.message));
                }
                if (!user) {
                    return rej(new Error("User not found"));
                }

                const { data, ...config } = makePayload(user);
                jwt.sign(data, config.secret, { algorithm: config.algorithm }, (error, token) => {
                    if (error) {
                        return new Error(error.message);
                    }

                    res(new Auth(user, token));
                })
            });
        })
    }

    middleware = (request: Request, response: AuthResponse, next: NextFunction): void => {
        passport.authenticate('jwt', { session: false }, async (error, user, info) => {
            response.auth = {
                info: null,
                error: null,
                token: null
            };

            if (this.debug && info && info.message) {
                // tslint:disable-next-line: no-console
                console.info("ensureJwt Info:", JSON.stringify(info.message));
            }

            if (info) {
                let message = null;

                switch (info.name) {
                    case "JsonWebTokenError":
                        if (info.message === "No auth token") {
                            return next();
                        }
                        message = "Malformed Token";
                        break;
                    case "TokenExpiredError":
                        message = "Has been expired";
                        break;
                    default:
                        message = info.message;
                }

                response.auth.info = new GqlError(`Auth exception, ${message}`, "AuthTokenError")
                return next();
            }

            if (error) {
                response.auth.error = new GqlError(error.message, error.code, error)
                return next();
            }

            response.auth.token = JWT_STRATEGY_OPTIONS.jwtFromRequest(request);

            this.strategy.deserialize()(user.email, () => null).then(user => {
                response.user = user;
                next();
            });
        })(request, response);
    }
}