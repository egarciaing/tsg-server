// eslint-disable-next-line @typescript-eslint/ban-types
export type IUser<U = {}> = {
    email: string;
    active: boolean;
    verify: boolean;
    salt: string;
    hash: string;
    id: number | string;
} & U;