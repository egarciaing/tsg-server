import { GraphQLSchema } from "graphql";
import GqlError from "../GqlError";
import { IUser } from "./User";

export const UNAME_KEY: keyof IUser = "email"

export type LocalFinder<U = Record<string, unknown>> = (email: string) => Promise<Partial<IUser<U>>>;

export interface AuthContext {
    info?: GqlError
    error?: GqlError
    token?: string
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface AuthResolverFn<T = any> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (context: T): T & Record<string, any>
}

export type AuthDirective = {
    DirectiveTransformer(schema: GraphQLSchema): GraphQLSchema;
    TypeDefs: string
}

export interface IAuth {
    user: IUser;
    token: string;
}

export class Auth implements IAuth {
    user: IUser = null;
    token: string = null;
    // tslint:disable-next-line: variable-name
    readonly __typename: string = "Auth";

    constructor(user: IUser, token: string) {
        this.user = user;
        this.token = token;
    }
}
