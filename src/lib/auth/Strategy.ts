import { Strategy as JwtStrategy } from 'passport-jwt';
import { Strategy as LocalStrategy, IStrategyOptions } from 'passport-local';

import { JWT_STRATEGY_OPTIONS, BLACK_BOX } from '../../config/auth/config';
import { LocalFinder, UNAME_KEY } from "./Auth";
import omitFields from '../omitFields';
import Password from './Password';
import Encoder, { Deserialize } from './Encoder';

type Next = (...args: unknown[]) => void;
type Options = { finder: LocalFinder } & IStrategyOptions;

export default class Strategy {
    private options: Options;
    deserialize: () => Deserialize;

    constructor(options: Options) {
        this.options = options;
        this.deserialize = () => Encoder.deserialize(options.finder);
    }

    authenticate(): (email: string, password: string, next: unknown) => void {
        return (email: string, password: string, next: Next): void => {
            this.options.finder(email)
                .then(user => {
                    if (!user) {
                        return next(null, false, { message: 'User not found' })
                    }

                    Password.has(password, { salt: user.salt, hash: user.hash })
                        .then(() => next(null, omitFields(user, BLACK_BOX)))
                        .catch(error => next(null, false, error));
                })
                .catch(error => next(null, false, error));
        }
    }

    createLocalStrategy(): LocalStrategy {
        return new LocalStrategy(
            this.options,
            this.authenticate()
        );
    }

    createJwtStrategy(): JwtStrategy {
        const finder = this.deserialize();
        return new JwtStrategy(
            JWT_STRATEGY_OPTIONS,
            (payload, next) => finder(payload[UNAME_KEY], next)
        );
    }
}