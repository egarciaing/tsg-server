import { Table, Column, Model, ForeignKey, BelongsTo, PrimaryKey } from 'sequelize-typescript';
import Address from '../../Location/Address';
import Person from '../People';

@Table
export default class PersonAddress extends Model<PersonAddress>{
    @PrimaryKey
    @ForeignKey(() => Address)
    @Column
    addressId: number;

    @BelongsTo(() => Address, 'addressId')
    address: Address

    @PrimaryKey
    @ForeignKey(() => Person)
    @Column
    personId: number;

    @BelongsTo(() => Person, 'personId')
    person: Person
}