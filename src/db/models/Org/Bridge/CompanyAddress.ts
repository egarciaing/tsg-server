import { Table, Column, Model, ForeignKey, BelongsTo, PrimaryKey } from 'sequelize-typescript';
import Address from '../../Location/Address';
import Company from '../Company';

@Table
export default class CompanyAddress extends Model<CompanyAddress>{
    @PrimaryKey
    @ForeignKey(() => Address)
    @Column
    addressId: number;

    @BelongsTo(() => Address, 'addressId')
    address: Address

    @PrimaryKey
    @ForeignKey(() => Company)
    @Column
    companyId: number;

    @BelongsTo(() => Company, 'companyId')
    company: Company
}