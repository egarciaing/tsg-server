import { Table, Column, Model, ForeignKey, BelongsTo, PrimaryKey } from 'sequelize-typescript';
import Phone from '../../Phone';
import Person from '../People';

@Table
export default class PersonPhones extends Model<PersonPhones>{
    @PrimaryKey
    @ForeignKey(() => Phone)
    @Column
    phoneId: number;

    @BelongsTo(() => Phone, 'phoneId')
    phone: Phone

    @PrimaryKey
    @ForeignKey(() => Person)
    @Column
    personId: number;

    @BelongsTo(() => Person, 'personId')
    person: Person
}