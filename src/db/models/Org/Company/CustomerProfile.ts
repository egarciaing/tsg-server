import { Table, Column, Model, PrimaryKey, Unique, DataType, BelongsTo, ForeignKey, Default, AllowNull, Is, DefaultScope, IsEmail } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';
import { Provider } from './EntityProvider';
import Employee from '../People/Employee';
import Company from './index';

type TargetFields = "isClientOfTheIntegratorId" | "isClientOfTheDistributorId";
const validateServiceProviderEntity = (otherField: TargetFields) => {
    return function (id: number): void {
        if (isNaN(id) && isNaN(this[otherField])) {
            throw new Error("Must be a customer of a distributor or integrator");
        }

        if (!isNaN(id) && !isNaN(this[otherField])) {
            throw new Error("You can only be a customer of a distributor or integrator");
        }
    }
}

@DefaultScope(() => ({
    include: [
        { model: Company, as: "entity", identifier: "companyId" },
        { model: Company, as: "distributor", identifier: "isClientOfTheDistributorId" },
        Employee
    ]
}))
@ObjectType()
@Table({
    paranoid: true
})
export default class CustomerProfile extends Model<CustomerProfile>{
    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @Unique
    @IsEmail
    @Field(() => String)
    @Column(DataType.STRING)
    email: string;

    @Field(() => String)
    @Column
    reviewed: string;

    @Default(0)
    @Field(() => Number)
    @Column(DataType.INTEGER)
    numberOfEmployees: number;

    @Unique
    @Field(() => Number)
    @Column(DataType.INTEGER)
    efactoryId: number;

    @ForeignKey(() => Company)
    @Column
    companyId: number;

    @BelongsTo(() => Company, "companyId")
    company: Company;

    // is client of...
    @Is("hasClientOfIntegrator", validateServiceProviderEntity("isClientOfTheDistributorId"))
    @ForeignKey(() => Employee)
    @AllowNull
    @Column
    isClientOfTheIntegratorId: number;

    @BelongsTo(() => Employee, "isClientOfTheIntegratorId")
    integrator?: Employee;

    @Is("hasClientOfDistributor", validateServiceProviderEntity("isClientOfTheIntegratorId"))
    @ForeignKey(() => Company)
    @AllowNull
    @Column
    isClientOfTheDistributorId: number;

    @BelongsTo(() => Company, "isClientOfTheDistributorId")
    distributor?: Company;
    // end: is client of...

    @Field(() => Provider)
    get provider(): Provider {
        if (this.integrator) {
            return this.integrator;
        }

        return this.distributor;
    }
}