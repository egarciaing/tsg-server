import { createUnionType } from "type-graphql";
import Compay from "../Company";
import Employee from "../People/Employee"

export type Provider = Employee | Compay;

export const Provider = createUnionType({
    name: "EntityProvider",
    types: () => [Employee, Compay] as const,
    resolveType: (value) => ("tenant" in value) ? Compay : Employee
});