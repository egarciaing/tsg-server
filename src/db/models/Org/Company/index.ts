import { Table, Column, Model, DataType, PrimaryKey, AllowNull, HasMany, DefaultScope, BelongsTo, ForeignKey, BeforeCreate, HasOne } from 'sequelize-typescript';
import { ObjectType, Field, registerEnumType, ID } from 'type-graphql';

import { enumValToArray } from '../../../../lib/enumToArray';
import CompanyAddress from '../Bridge/CompanyAddress';
import Address from '../../Location/Address';
// import Phone from '../Phone';
import CustomerProfile from './CustomerProfile';
import Employee from '../People/Employee';
import Tenant from '../Tenant';

export enum COMPANY_TYPE {
    OWNER = "Owner",
    CLIENT = "Client",
    DISTRIBUTOR = "Distributor"
}

registerEnumType(COMPANY_TYPE, {
    name: "COMPANY_TYPE",
    description: "Company role type into the system"
})

@DefaultScope(() => ({
    include: [
        { model: Tenant, attributes: ["token", "description"] },
        { model: Address, attributes: ["name","address",] }
    ]
}))
@ObjectType()
@Table({
    paranoid: true
})
export default class Company extends Model<Company>{
    @PrimaryKey
    @Field(()=> ID)
    @Column
    id: number;

    @Field(() => String)
    @Column(DataType.STRING)
    name: string;

    @AllowNull
    @Field(() => String, { nullable: true })
    @Column(DataType.TEXT)
    activity?: string;

    @Field(() => Date)
    @Column(DataType.DATE)
    dateOfBirth: Date;

    @Field(() => COMPANY_TYPE)
    @Column(DataType.ENUM(...enumValToArray(COMPANY_TYPE)))
    type: COMPANY_TYPE;

    @ForeignKey(() => Tenant)
    @Column
    tenantId: number;

    @Field(() => Tenant)
    @BelongsTo(() => Tenant, "tenantId")
    tenant: Tenant;

    @HasMany(() => CompanyAddress)
    addresses: CompanyAddress[];

    @HasMany(() => Employee)
    employees: Employee[];

    @HasOne(() => CustomerProfile, "companyId")
    customerProfile?: CustomerProfile;

    @HasMany(() => CustomerProfile, "isClientOfTheDistributor")
    clients: CustomerProfile[];

    @BeforeCreate
    static checkIfRoleAcceptsIntegrator(instance: Company): void {
        if (instance.type === COMPANY_TYPE.CLIENT) {
            if (instance.customerProfile === null) {
                throw new Error(`The customer profile is mandatory for the client company. but [${typeof instance.customerProfile}] was found`)
            }
            instance.employees = [];
        }
    }
}