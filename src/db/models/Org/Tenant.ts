import { Table, Column, Model, DataType, PrimaryKey, AllowNull, Default, HasOne, Unique, Index, HasMany } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';
import Company from './Company';
import Person from './People';

export interface ITenant {
    id: number;
    token: string;
    description?: string;
}

@ObjectType()
@Table({
    paranoid: true
})
export default class Tenant extends Model<Tenant> implements ITenant {
    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @AllowNull
    @Field(() => String, { nullable: true })
    @Column(DataType.STRING)
    description?: string;

    @Unique
    @Index
    @Column(DataType.STRING)
    token: string;

    @Default(true)
    @Field(() => Boolean)
    @Column(DataType.BOOLEAN)
    active: boolean;

    @Field(() => Company)
    @HasOne(() => Company)
    compay: Company

    @Field(() => [Person], { nullable: "items" })
    @HasMany(() => Person)
    people: Person[]
}