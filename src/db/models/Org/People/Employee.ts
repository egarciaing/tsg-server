import { Table, Column, Model, ForeignKey, DataType, Default, BelongsTo, DefaultScope, PrimaryKey, AllowNull, HasMany } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';
import Company from '../Company';
import Person from '.';
import CustomerProfile from '../Company/CustomerProfile';

@DefaultScope(() => ({
    include: [CustomerProfile]
}))
@ObjectType()
@Table
export default class Employee extends Model<Employee>{

    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @Field(() => Number)
    @ForeignKey(() => Company)
    @Column
    employerId: number;

    @BelongsTo(() => Company, 'employerId')
    employer: Company

    @PrimaryKey
    @Field(() => Number)
    @ForeignKey(() => Person)
    @Column
    personId: number;

    @BelongsTo(() => Person, 'personId')
    person: Person;

    @AllowNull
    @Field(() => String)
    @Column(DataType.STRING)
    jobplace?: string;

    @AllowNull
    @Default(true)
    @Field(() => Boolean)
    @Column(DataType.BOOLEAN)
    active: boolean;

    @Default(false)
    @Field(() => Boolean)
    @Column(DataType.BOOLEAN)
    isIntegrator: boolean;

    @Field(() => [CustomerProfile], { nullable: "itemsAndList" })
    @HasMany(() => CustomerProfile, "isClientOfTheIntegrator")
    clients?: CustomerProfile[];
}