import { Table, Column, Model, DataType, PrimaryKey, AllowNull, HasMany, ForeignKey, BelongsTo, HasOne, Scopes } from 'sequelize-typescript';
import { Field, ObjectType, registerEnumType, ID } from 'type-graphql';
import { enumValToArray } from "../../../../lib/enumToArray";
import Address from '../../Location/Address';
import Phone from '../../Phone';
import User from '../../User';

import PersonAddress from '../Bridge/PersonAddress';
import PersonPhones from '../Bridge/PersonPhones';
import Tenant from '../Tenant';
import Employee from './Employee';

export enum GENDER {
    MALE = "Male",
    FEMALE = "Female"
}

registerEnumType(GENDER, {
    name: "GENDER",
    description: "People gender options"
})

@Scopes(() => ({
    tenant: {
        include: [{ model: Tenant, attributes: ["token", "description"] }],
        limit: 1,
    },
    location: {
        include: [Address, Phone],
        attributes: ["firstname", "lastname", "dateOfBirth", "gender"],
    },
    employee: {
        include: [{ model: Employee, attributes: ["jobplace", "active", "isIntegrator"] }],
        attributes: ["firstname", "lastname", "dateOfBirth", "gender"],
    }
}))
@ObjectType()
@Table({
    paranoid: true
})
export default class Person extends Model<Person>{

    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @Field(() => String)
    @Column(DataType.STRING(100))
    firstname: string;

    @AllowNull
    @Field(() => String, { nullable: true })
    @Column(DataType.STRING(100))
    lastname?: string;

    @Field(() => Date)
    @Column(DataType.DATE)
    dateOfBirth: Date;

    @Field(() => GENDER, { description: "Gender, Male or Female" })
    @Column(DataType.ENUM(...enumValToArray(GENDER) as string[]))
    gender: GENDER;

    @HasMany(() => PersonAddress)
    addresses: PersonAddress[];

    @HasMany(() => PersonPhones)
    phones: PersonPhones[];

    @HasOne(() => Employee, { foreignKey: "personId" })
    hasEmployee?: Employee;

    @ForeignKey(() => Tenant)
    @Column({ comment: "Tenant ID for manager people" })
    tenantId: number;

    @BelongsTo(() => Tenant)
    tenant: Tenant;

    @AllowNull
    @ForeignKey(() => User)
    @Column({ comment: "User profile Id" })
    userId?: number;

    @BelongsTo(() => User, "userId")
    userProfile?: User;
}
