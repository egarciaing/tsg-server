import { Table, Column, Model, DataType, PrimaryKey, Unique, BelongsToMany, DefaultScope, HasOne, IsEmail } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';

import Permission from './Authorization/Permission';
import Role from './Authorization/Role';
import UserPermissions from './Authorization/Bridge/UserPermissions';
import UserRoles from './Authorization/Bridge/UserRoles';
import Person from './Org/People';
// tools
import { PasswordChunk } from '../../lib/auth/Password';
import { IUser } from '../../lib/auth';

@DefaultScope(() => ({
    include: [Role, Permission]
}))
@ObjectType({
    description: "User of system"
})
@Table({
    paranoid: true
})
export default class User extends Model<User> implements IUser {

    @PrimaryKey
    @Field(() => ID)
    @Column(DataType.INTEGER)
    id: number;

    @Unique
    @IsEmail
    @Field(() => String)
    @Column(DataType.STRING(123))
    email: string;

    @Column
    salt: string;

    @Column
    hash: string;

    @Field(() => Boolean)
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: true
    })
    verify: boolean;

    @Column({ allowNull: true })
    activationKey?: string;

    @Column({ allowNull: true })
    resetPasswordKey?: string;

    @Field(() => Boolean)
    @Column({
        type: DataType.BOOLEAN,
        defaultValue: true
    })
    active: boolean;

    @Field(() => [Permission], { nullable: "items" })
    @BelongsToMany(() => Permission, () => UserPermissions)
    permissions: Permission[];

    @Field(() => [Role], { nullable: "items" })
    @BelongsToMany(() => Role, () => UserRoles)
    roles: Role[];

    @Field(() => Person, { nullable: true })
    @HasOne(() => Person)
    owner: Person;

    get isActive(): boolean {
        return this.active && (this.activationKey === null);
    }

    public activate(): void {
        this.active = true;
    }

    public deactivate(): void {
        this.active = false;
    }

    public forgotPassword(settoken: string): void {
        this.setDataValue("resetPasswordKey", settoken);
    }
    public resetPassword(newPassword: PasswordChunk, token: string): void {
        if (this.resetPasswordKey === token) {
            this.setDataValue("salt", newPassword.salt);
            this.setDataValue("hash", newPassword.hash);
        }
    }
    public setPassword(password: PasswordChunk): void {
        this.setDataValue("salt", password.salt);
        this.setDataValue("hash", password.hash);
    }

    public setVerificationToken(settoken: string): void {
        this.setDataValue("activationKey", settoken);
    }
    public verifyToken(token: string | boolean): void {
        if (this.activationKey === token) {
            this.setDataValue("activationKey", null);
            this.setDataValue("verify", true);
            this.setDataValue("active", true);
        }
    }

    public hasRole(name: string): boolean {
        return this.roles.findIndex((permission) => permission.name === name) > -1;
    }

    public hasPermission(name: string): boolean {
        return this.permissions.findIndex((permission) => permission.name === name) > -1;
    }
}