import { Table, Column, Model, DataType, PrimaryKey, DefaultScope, BelongsTo, ForeignKey, HasOne } from 'sequelize-typescript';
import { Field, ObjectType, registerEnumType, ID } from 'type-graphql';
import { enumValToArray } from "../../lib/enumToArray";
import Country from './Location/Country';
import PersonPhones from './Org/Bridge/PersonPhones';

export enum PHONE_TYPE {
    LOCAL = "Local",
    MOBILE = "Mobile"
}

registerEnumType(PHONE_TYPE, {
    name: "PHONE_TYPE",
    description: "Describe if the phone is mobile or local"
})

@DefaultScope(() => ({
    include: [{ model: Country, attributes: ["phonecode"] }]
}))
@ObjectType({
    description:"Telephone numbers in international format"
})
@Table
export default class Phone extends Model<Phone>{

    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @Field(() => String)
    @Column({
        allowNull: false,
        type: DataType.STRING(100)
    })
    number: string;

    @Field(() => PHONE_TYPE)
    @Column(DataType.ENUM(...enumValToArray(PHONE_TYPE) as string[]))
    type: PHONE_TYPE;

    @ForeignKey(() => Country)
    @Column
    countryId: number;

    @BelongsTo(() => Country)
    country: Country;

    @HasOne(() => PersonPhones, { as: 'owner' })
    owner?: PersonPhones;
}
