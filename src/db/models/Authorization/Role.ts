import { Table, Column, Model, DataType, AllowNull, BeforeCreate, PrimaryKey, BelongsToMany, DefaultScope, Default, Comment } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';

import { enumValToArray } from "../../../lib/enumToArray";
import RolePermissions from './Bridge/RolePermissions';
import UserRoles from './Bridge/UserRoles';
import Permission from './Permission';
import User from '../User';

export enum ROLE_SPECIAL {
    ALLOW_ALL = "ALLOW_ALL",
    DENY_ALL = "DENY_ALL"
}

export enum ROLE_TARGET {
    User = "User",
    Company = "Company"
}


@DefaultScope(() => ({
    include: [Permission]
}))
@ObjectType()
@Table({
    paranoid: true
})
export default class Role extends Model<Role>{

    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @Field(() => String)
    @Column(DataType.STRING(123))
    name: string;

    @Field(() => String)
    @Column({
        allowNull: true,
        type: DataType.STRING
    })
    description?: string;

    @AllowNull
    @Comment("Override all permissions from case")
    @Column(DataType.ENUM(...enumValToArray(ROLE_SPECIAL) as string[]))
    special?: ROLE_SPECIAL;

    @Default(ROLE_TARGET.User)
    @Comment("Used to filter cases. Default is User")
    @Column(DataType.ENUM(...enumValToArray(ROLE_TARGET) as string[]))
    target: ROLE_TARGET;

    @Field(() => [Permission], { nullable: "items" })
    @BelongsToMany(() => Permission, () => RolePermissions)
    permissions: Permission[];

    @BelongsToMany(() => User, () => UserRoles)
    users: User[];

    @BeforeCreate
    static normalizeName(instance: Role): void {
        instance.name = instance.name.trim().replace(/\s/, "-");
    }

    public hasPermission(name: string): boolean {
        return this.permissions.findIndex((permission) => permission.name === name) > -1
    }
}