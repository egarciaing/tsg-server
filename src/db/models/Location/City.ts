import { Table, Column, Model, DataType, PrimaryKey, BelongsTo, HasMany, ForeignKey, AllowNull } from 'sequelize-typescript';
import { Field, ObjectType,ID } from 'type-graphql';
import Address from './Address';
import State from './State';

@ObjectType()
@Table
export default class City extends Model<City>{

    @PrimaryKey
    @Field(() => ID)
    @Column
    id: number;

    @AllowNull
    @Field(() => String)
    @Column(DataType.STRING(6))
    zip?: string;

    @Column(DataType.STRING(123))
    @Field(() => String)
    name: string;

    @ForeignKey(() => State)
    @Column
    state_id: number;

    @Field(() => State)
    @BelongsTo(() => State, "state_id")
    state: State

    @HasMany(() => Address)
    addresses: Address[];
}
