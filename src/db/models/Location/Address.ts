import { Table, Column, Model, DataType, PrimaryKey, Comment, BelongsTo, ForeignKey, AllowNull, HasOne } from 'sequelize-typescript';
import { Field, ObjectType, ID } from 'type-graphql';
import CompanyAddress from '../Org/Bridge/CompanyAddress';
import PersonAddress from '../Org/Bridge/PersonAddress';
import City from './City';

export enum ADDRESSS_TYPE {
    RESIDENTIAL = "Residential",
    OFFICE = "Office"
}

@ObjectType()
@Table
export default class Address extends Model<Address>{

    @PrimaryKey
    @Field(()=> ID)
    @Column
    id: number;

    @Comment("Name identifytier for the addres")
    @AllowNull
    @Field(()=> String)
    @Column(DataType.STRING(123))
    name?: string;

    @Comment("Specific address")
    @Field(()=> String)
    @Column(DataType.STRING)
    address: string;

    @ForeignKey(() => City)
    @Column
    cityId: number;

    @BelongsTo(() => City)
    city: City;

    @HasOne(() => PersonAddress)
    person?: PersonAddress;

    @HasOne(() => CompanyAddress)
    company?: CompanyAddress;
}
