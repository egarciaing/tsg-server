import { Sequelize } from 'sequelize-typescript';
import config from "../config/sequelize";
// Entities
import RolePermissions from './models/Authorization/Bridge/RolePermissions';
import UserPermissions from './models/Authorization/Bridge/UserPermissions';
import UserRoles from './models/Authorization/Bridge/UserRoles';
import Permission from './models/Authorization/Permission';
import Role from './models/Authorization/Role';
import Address from './models/Location/Address';
import City from './models/Location/City';
import Country from './models/Location/Country';
import State from './models/Location/State';
import Employees from './models/Org/People/Employee';
import Company from './models/Org/Company';
import Person from './models/Org/People';
import Phone from './models/Phone';
import User from './models/User';
import Tenant from './models/Org/Tenant';
import PersonAddress from './models/Org/Bridge/PersonAddress';
import PersonPhones from './models/Org/Bridge/PersonPhones';
import CustomerProfile from './models/Org/Company/CustomerProfile';
import CompanyAddress from './models/Org/Bridge/CompanyAddress';

const sequelize = (config?.USE_ENV_VARIABLE) ?
    new Sequelize(process.env[config?.USE_ENV_VARIABLE]) :
    new Sequelize(config.database, config.username, config.password, config);


sequelize.addModels([
    Tenant,
    User,
    Phone,
    Address, Country, State, City,
    Role, Permission, UserRoles, UserPermissions, RolePermissions,
    Person, PersonAddress, PersonPhones, Employees,
    Company, CompanyAddress, CustomerProfile,
]);

export default sequelize;
