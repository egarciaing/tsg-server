import { Seed } from '../../../bin/seed';

export const up: Seed = async ({ context: queryInterface }) => queryInterface.bulkInsert("Tenants", [
    {
        id:1,
        token: "f0n4x",
        description: "Venezuela, Fonax c.a",
        createdAt: new Date,
        updatedAt: new Date,
    }
], {});

export const down: Seed = async ({ context: queryInterface }) => queryInterface.bulkDelete("Tenants", null, {});
