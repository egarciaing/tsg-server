import { COMPANY_TYPE } from "../models/Org/Company";
import { Seed } from '../../../bin/seed';

export const up: Seed = async ({ context: queryInterface }) => queryInterface.bulkInsert("Companies", [
    {
        id:1,
        name: "Fonax c.a",
        activity: "Telecomunicaciones",
        dateOfBirth: new Date(),
        type: COMPANY_TYPE.OWNER,
        tenantId: 1,
        createdAt: new Date,
        updatedAt: new Date,
    }
], {});

export const down: Seed = async ({ context: queryInterface }) => queryInterface.bulkDelete("Companies", null, {});
