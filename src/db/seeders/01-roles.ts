import { ROLE_SPECIAL, ROLE_TARGET } from "../models/Authorization/Role";
import { Seed } from '../../../bin/seed';

export const up: Seed = async ({ context: queryInterface }) => queryInterface.bulkInsert("Roles", [
    {
        id:1,
        name: "root",
        description: "all permissions granted",
        special: ROLE_SPECIAL.ALLOW_ALL,
        target: ROLE_TARGET.User,
        createdAt: new Date,
        updatedAt: new Date,
    }
], {});

export const down: Seed = async ({ context: queryInterface }) => queryInterface.bulkDelete("Roles", null, {});
