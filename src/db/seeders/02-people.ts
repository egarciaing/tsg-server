import { GENDER } from "../models/Org/People";
import Password from "../../lib/auth/Password";
import { Seed } from '../../../bin/seed';

export const up: Seed = async ({ context: queryInterface }) => {

    await Password.make("password", { keylen: 100 }).then((password) => queryInterface.bulkInsert("Users", [
        {
            id: 1,
            email: "admin@fonax.com",
            ...password,
            createdAt: new Date,
            updatedAt: new Date,
        }
    ]))

    return queryInterface.bulkInsert("People", [
        {
            id: 1,
            firstname: "John",
            lastname: "Smith",
            dateOfBirth: new Date(),
            gender: GENDER.MALE,
            tenantId: 1,
            userId: 1,
            createdAt: new Date,
            updatedAt: new Date,
        }
    ]).then(() => queryInterface.bulkInsert("Employees", [
        {
            id: 1,
            personId: 1,
            employerId: 1,
            jobplace: "Tecnical Manager",
            active: true,
            isIntegrator: false,
            createdAt: new Date,
            updatedAt: new Date,
        }
    ]));
};

export const down: Seed = async ({ context: queryInterface }) => queryInterface.bulkDelete("People", null, {});
