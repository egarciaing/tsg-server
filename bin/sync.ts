import { isProdMode } from "../src/config/env";
import db from "../src/db";

(async () => {
    await db.sync({
        alter: isProdMode,
        force: !isProdMode,
        benchmark: !isProdMode
    });

    console.log("Database schema has been sync");
})();