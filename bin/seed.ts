require('ts-node/register');
import { makeUmzug } from "./umzug";

const umzug = makeUmzug("../src/db/seeders/**/*.{ts,js}");

export type Seed = typeof umzug._types.migration;

(async () => {
    await umzug.up();
})();