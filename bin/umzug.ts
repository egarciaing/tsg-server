import { QueryInterface, Sequelize } from 'sequelize';
import { Umzug, SequelizeStorage } from 'umzug';
import config from "../src/config/sequelize";

export const sequelize = (config?.USE_ENV_VARIABLE) ?
    new Sequelize(process.env[config?.USE_ENV_VARIABLE]) :
    new Sequelize(config.database, config.username, config.password, config);

export const makeUmzug = (path: string): Umzug<QueryInterface> => new Umzug({
    migrations: { glob: path },
    context: sequelize.getQueryInterface(),
    storage: new SequelizeStorage({ sequelize }),
    logger: console,
});
