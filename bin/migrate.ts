require('ts-node/register');
import { makeUmzug } from "./umzug";

const umzug = makeUmzug("../src/db/migrations/**/*.{ts,js}");

export type Migration = typeof umzug._types.migration;

(async () => {
    await umzug.up();
})();